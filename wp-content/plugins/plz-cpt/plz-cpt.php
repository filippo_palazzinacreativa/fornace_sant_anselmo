<?php
/*
Plugin Name: PLZ Custom post type
Description: plugin per la realizzazione di un custom post type da codice
*/



/* Start Adding Functions Below this Line */

//post type ambienti
function ambienti_custom_post() {
    // creating (registering) the custom type
    register_post_type( 'ambienti', /* custom post type name */
    // let's now add all the options for this post type
    array('labels' => array(
    'name' => 'Ambienti', /* general name for the post type, usually plural. */
    'singular_name' => 'ambiente', /* name for one object of this post type. */
    'all_items' => 'Tutti gli ambienti', /* the all items text used in the menu. */
    'add_new' => 'Aggiungi nuovo', /* the add new text. */
    'add_new_item' => 'Aggiungi nuovo ambiente', /* the add new item text */
    'edit_item' => 'Modifica Post Types', /*  the edit item text */
    'new_item' => 'Nuovo Post Type', /* the new item text */
    'view_item' => 'Vedi Post Type', /* the view item text */
    'search_items' => 'Cerca Post Type', /* the search items text */
    'not_found' =>  'Il database è vuoto, aggiungi ambienti', /* the not found text */
    'not_found_in_trash' => 'Nothing found in Trash', /* the not found in trash text */
    'parent_item_colon' => ''
    ), /* end of arrays */
    'description' => 'ambienti Collection Page', /* a short descriptive summary of what the post type is. */
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'show_ui' => true, /* Whether to generate a default UI for managing this post type in the admin */
    'query_var' => true,
    'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
    'menu_icon' => 'dashicons-store', /* the icon for the custom post type menu */
    'rewrite'	=> array( 'slug' => 'ambienti', 'with_front' => false ), /* you can specify its url slug */
    'has_archive' => 'ambienti', /* you can rename the slug here */
    'capability_type' => 'post',
    'hierarchical' => false,
    /* the next one is important, it tells what's enabled in the post editor */
    'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'sticky')
    ) /* end of options */
    ); /* end of register post type */
    
    
    $labels_cat = array(
    'name' => _x( 'categorie', 'taxonomy general name' ),
    'singular_name' => _x( 'categoria', 'taxonomy singular name' ),
    'search_items' =>  __( 'Cerca categorie' ),
    'all_items' => __( 'Tutte le categorie' ),
    'parent_item' => __( 'Parent categorie' ),
    'parent_item_colon' => __( 'Parent categorie:' ),
    'edit_item' => __( 'Modifica categoria' ),
    'update_item' => __( 'Update categorie' ),
    'add_new_item' => __( 'Aggiungi nuova categoria' ),
    'new_item_name' => __( 'Nome nuova categoria' ),
    'menu_name' => __( 'categorie' ),
    );
    
    register_taxonomy('categorie_ambienti',array('ambienti'), array(
    'hierarchical' => true,
    'labels' => $labels_cat,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'categorie-ambienti' ),
    ));
    
    
    $labels_cat = array(
    'name' => _x( 'tag', 'taxonomy general name' ),
    'singular_name' => _x( 'tag', 'taxonomy singular name' ),
    'search_items' =>  __( 'Cerca tag' ),
    'all_items' => __( 'Tutti i tag' ),
    'parent_item' => __( 'Parent tag' ),
    'parent_item_colon' => __( 'Parent tag:' ),
    'edit_item' => __( 'Modifica tag' ),
    'update_item' => __( 'Update tag' ),
    'add_new_item' => __( 'Aggiungi nuovi tag' ),
    'new_item_name' => __( 'Nome nuovo tag' ),
    'menu_name' => __( 'tag' ),
    );
    
    register_taxonomy('tag_ambienti',array('ambienti'), array(
    'hierarchical' => true,
    'labels' => $labels_cat,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'tag-ambienti' ),
    ));
    
    
}

// adding the function to the Wordpress init
add_action( 'init', 'ambienti_custom_post');

add_filter( 'rwmb_meta_boxes', 'plz_ambiente_product_meta_boxes' );
function plz_ambiente_product_meta_boxes( $meta_boxes ) {

    $args = array( 'post_type' => 'product' ,'posts_per_page' => 100000);
    $products = get_posts( $args );
    $array_prodotti = array("" => "Seleziona prodotto");
    foreach( $products as $product ) : 
     $array_prodotti [$product->ID]= $product->post_title;
    endforeach;

    // 1st meta box
    $meta_boxes[] = array(
        'title'      => __( 'Prodotto ambiente', 'palazzinatheme' ),
        'post_types' => 'ambienti',
        'fields' => array(
          array(
                            'name' => __( "Prodotto collegato all'ambiente", 'palazzinatheme' ),
                            'id'   => 'plz_ambiente_prodotto',
                            'type' => 'select',
                            'options' => $array_prodotti,
                      ),
        )
    );
    return $meta_boxes;
}
/*
add_filter( 'rwmb_meta_boxes', 'plz_image_upload_ambiente_categoria' );
function plz_image_upload_ambiente_categoria( $meta_boxes ) {
    // 1st meta box
    $meta_boxes[] = array(
        'title'      => __( 'immagine categoria', 'palazzinatheme' ),
        'post_types' => 'categorie_ambienti',
        'fields' => array(
          array(
                            'name' => __( 'Immagine slide', 'palazzinatheme' ),
                            'id'   => 'plz_img_categoria_ambiente',
                            'type' => 'image_advanced',
                      ),
        )
    );
    return $meta_boxes;
}
*/
/* Stop Adding Functions Below this Line */

add_action( 'init', 'create_categorie_ambienti_nonhierarchical_taxonomy', 0 );

function create_categorie_ambienti_nonhierarchical_taxonomy() {

// Labels part for the GUI

  $labels = array(
    'name' => _x( 'categorie_ambienti', 'taxonomy general name' ),
    'singular_name' => _x( 'Writer', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search categorie_ambienti' ),
    'popular_items' => __( 'Popular categorie_ambienti' ),
    'all_items' => __( 'All categorie_ambienti' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Writer' ), 
    'update_item' => __( 'Update Writer' ),
    'add_new_item' => __( 'Add New Writer' ),
    'new_item_name' => __( 'New Writer Name' ),
    'separate_items_with_commas' => __( 'Separate categorie_ambienti with commas' ),
    'add_or_remove_items' => __( 'Add or remove categorie_ambienti' ),
    'choose_from_most_used' => __( 'Choose from the most used categorie_ambienti' ),
    'menu_name' => __( 'categorie_ambienti' ),
  ); 

// Now register the non-hierarchical taxonomy like tag

  register_taxonomy('categorie_ambienti','post',array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'show_in_nav_menus' =>  true,
    'has_archive'       =>  true,
    'rewrite' => array( 'slug' => 'writer', 'with_front' => false),
  ));
function writer_flush_rewrite() {
    global $wp_rewrite;
    $wp_rewrite->flush_rules();
}
add_action('init', 'writer_flush_rewrite');
}

/**
 * Plugin class
 **/
function load_wp_media_files() {
  wp_enqueue_media();
}
add_action( 'admin_enqueue_scripts', 'load_wp_media_files' );

if ( ! class_exists( 'CT_TAX_META' ) ) {

class CT_TAX_META {

  public function __construct() {
    //
  }

 /*
  * Initialize the class and start calling our hooks and filters
  * @since 1.0.0
 */
 public function init() {
   add_action( 'categorie_ambienti_add_form_fields', array ( $this, 'add_categorie_ambienti_image' ), 10, 2 );
   add_action( 'created_categorie_ambienti', array ( $this, 'save_categorie_ambienti_image' ), 10, 2 );
   add_action( 'categorie_ambienti_edit_form_fields', array ( $this, 'update_categorie_ambienti_image' ), 10, 2 );
   add_action( 'edited_categorie_ambienti', array ( $this, 'updated_categorie_ambienti_image' ), 10, 2 );
   add_action( 'admin_footer', array ( $this, 'add_script' ) );
 }

 /*
  * Add a form field in the new categorie_ambienti page
  * @since 1.0.0
 */
 public function add_categorie_ambienti_image ( $taxonomy ) { ?>
   <div class="form-field term-group">
     <label for="categorie_ambienti-image-id"><?php _e('Image', 'hero-theme'); ?></label>
     <input type="hidden" id="categorie_ambienti-image-id" name="categorie_ambienti-image-id" class="custom_media_url" value="">
     <div id="categorie_ambienti-image-wrapper"></div>
     <p>
       <input type="button" class="button button-secondary ct_tax_media_button" id="ct_tax_media_button" name="ct_tax_media_button" value="<?php _e( 'Add Image', 'hero-theme' ); ?>" />
       <input type="button" class="button button-secondary ct_tax_media_remove" id="ct_tax_media_remove" name="ct_tax_media_remove" value="<?php _e( 'Remove Image', 'hero-theme' ); ?>" />
    </p>
   </div>
 <?php
 }

 /*
  * Save the form field
  * @since 1.0.0
 */
 public function save_categorie_ambienti_image ( $term_id, $tt_id ) {
   if( isset( $_POST['categorie_ambienti-image-id'] ) && '' !== $_POST['categorie_ambienti-image-id'] ){
     $image = $_POST['categorie_ambienti-image-id'];
     add_term_meta( $term_id, 'categorie_ambienti-image-id', $image, true );
   }
 }

 /*
  * Edit the form field
  * @since 1.0.0
 */
 public function update_categorie_ambienti_image ( $term, $taxonomy ) { ?>
   <tr class="form-field term-group-wrap">
     <th scope="row">
       <label for="categorie_ambienti-image-id"><?php _e( 'Image', 'hero-theme' ); ?></label>
     </th>
     <td>
       <?php $image_id = get_term_meta ( $term -> term_id, 'categorie_ambienti-image-id', true ); ?>
       <input type="hidden" id="categorie_ambienti-image-id" name="categorie_ambienti-image-id" value="<?php echo $image_id; ?>">
       <div id="categorie_ambienti-image-wrapper">
         <?php if ( $image_id ) { ?>
           <?php echo wp_get_attachment_image ( $image_id, 'slider-small' ); ?>
         <?php } ?>
       </div>
       <p>
         <input type="button" class="button button-secondary ct_tax_media_button" id="ct_tax_media_button" name="ct_tax_media_button" value="<?php _e( 'Add Image', 'hero-theme' ); ?>" />
         <input type="button" class="button button-secondary ct_tax_media_remove" id="ct_tax_media_remove" name="ct_tax_media_remove" value="<?php _e( 'Remove Image', 'hero-theme' ); ?>" />
       </p>
     </td>
   </tr>
 <?php
 }

/*
 * Update the form field value
 * @since 1.0.0
 */
 public function updated_categorie_ambienti_image ( $term_id, $tt_id ) {
   if( isset( $_POST['categorie_ambienti-image-id'] ) && '' !== $_POST['categorie_ambienti-image-id'] ){
     $image = $_POST['categorie_ambienti-image-id'];
     update_term_meta ( $term_id, 'categorie_ambienti-image-id', $image );
   } else {
     update_term_meta ( $term_id, 'categorie_ambienti-image-id', '' );
   }
 }

/*
 * Add script
 * @since 1.0.0
 */
 public function add_script() { ?>
   <script>
     jQuery(document).ready( function($) {
       function ct_media_upload(button_class) {
         var _custom_media = true,
         _orig_send_attachment = wp.media.editor.send.attachment;
         $('body').on('click', button_class, function(e) {
           var button_id = '#'+$(this).attr('id');
           var send_attachment_bkp = wp.media.editor.send.attachment;
           var button = $(button_id);
           _custom_media = true;
           wp.media.editor.send.attachment = function(props, attachment){
             if ( _custom_media ) {
               $('#categorie_ambienti-image-id').val(attachment.id);
               $('#categorie_ambienti-image-wrapper').html('<img class="custom_media_image" src="" style="margin:0;padding:0;max-height:100px;float:none;" />');
               $('#categorie_ambienti-image-wrapper .custom_media_image').attr('src',attachment.sizes.thumbnail.url).css('display','block');
                var src = attachment.url;

               if (attachment.sizes.thumbnail) {
                 src = attachment.sizes.thumbnail.url;
               }
               $('#categorie_ambienti-image-wrapper .custom_media_image').attr('src',src).css('display','block');
             } else {
               return _orig_send_attachment.apply( button_id, [props, attachment] );
             }
            }
         wp.media.editor.open(button);
         return false;
       });
     }
     ct_media_upload('.ct_tax_media_button.button'); 
     $('body').on('click','.ct_tax_media_remove',function(){
       $('#categorie_ambienti-image-id').val('');
       $('#categorie_ambienti-image-wrapper').html('<img class="custom_media_image" src="" style="margin:0;padding:0;max-height:100px;float:none;" />');
     });
     // Thanks: http://stackoverflow.com/questions/15281995/wordpress-create-categorie_ambienti-ajax-response
     $(document).ajaxComplete(function(event, xhr, settings) {
       var queryStringArr = settings.data.split('&');
       if( $.inArray('action=add-tag', queryStringArr) !== -1 ){
         var xml = xhr.responseXML;
         $response = $(xml).find('term_id').text();
         if($response!=""){
           // Clear the thumb image
           $('#categorie_ambienti-image-wrapper').html('');
         }
       }
     });
   });
 </script>
 <?php }

  }

$CT_TAX_META = new CT_TAX_META();
$CT_TAX_META -> init();

}
?>