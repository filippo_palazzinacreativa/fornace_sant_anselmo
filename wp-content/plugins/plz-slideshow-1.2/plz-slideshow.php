<?php
/*
Plugin Name: PLZ slideshow
Description: plugin per la realizzazione di uno slideshow custom
Version: 1.2
*/

function plz_register_styles() {
wp_enqueue_style('plz_slideshow-style', WP_PLUGIN_URL.'/plz-slideshow-1.2/slick.css');
wp_enqueue_style('plz_slideshow-style-photoswipe', WP_PLUGIN_URL.'/plz-slideshow-1.2/photoswipe/photoswipe.css');
wp_enqueue_style('plz_slideshow-style-photoswipe-skin', WP_PLUGIN_URL.'/plz-slideshow-1.2/photoswipe/default-skin.css');
wp_enqueue_script('plz_slideshow-slick', WP_PLUGIN_URL . '/plz-slideshow-1.2/jquery-1-12-4.js', array('jquery'), false, true);
wp_enqueue_script('plz_slideshow-jquery', WP_PLUGIN_URL . '/plz-slideshow-1.2/jquery-migrate-1.2.1-min.js', array('jquery'), false, true);
wp_enqueue_script('plz_slideshow-jquery-migrate', WP_PLUGIN_URL . '/plz-slideshow-1.2/slick.min.js', array('jquery'), false, true);
wp_enqueue_script('plz_slideshow-photoswype', WP_PLUGIN_URL . '/plz-slideshow-1.2/photoswipe/photoswipe.min.js', array('jquery'), false, true);
wp_enqueue_script('plz_slideshow-photoswype-default', WP_PLUGIN_URL . '/plz-slideshow-1.2/photoswipe/photoswipe-ui-default.min.js', array('jquery'), false, true);
wp_enqueue_script('plz_slideshow-custom', WP_PLUGIN_URL . '/plz-slideshow-1.2/custom.js', array('jquery'), false, true);
}
add_action( 'wp_enqueue_scripts', 'plz_register_styles' );


//post type slide
function plz_slide_custom_post() {
    // 	creating (registering) the custom type
    register_post_type( 'slide', /* custom post type name */
    // 	let's now add all the options for this post type
    array('labels' => array(
    'name' => 'slide', /* general name for the post type, usually plural. */
    'singular_name' => 'slide', /* name for one object of this post type. */
    'all_items' => 'Tutti i slide', /* the all items text used in the menu. */
    'add_new' => 'Aggiungi nuovo', /* the add new text. */
    'add_new_item' => 'Aggiungi nuovo slide', /* the add new item text */
    'edit_item' => 'Modifica Post Types', /*  the edit item text */
    'new_item' => 'Nuovo Post Type', /* the new item text */
    'view_item' => 'Vedi Post Type', /* the view item text */
    'search_items' => 'Cerca Post Type', /* the search items text */
    'not_found' =>  'Il database è vuoto, aggiungi slide', /* the not found text */
    'not_found_in_trash' => 'Nothing found in Trash', /* the not found in trash text */
    'parent_item_colon' => ''
    ), /* end of arrays */
    'description' => 'slide Collection Page', /* a short descriptive summary of what the post type is. */
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'show_ui' => true, /* Whether to generate a default UI for managing this post type in the admin */
    'query_var' => true,
    'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
    'menu_icon' => 'dashicons-store', /* the icon for the custom post type menu */
    'rewrite'	=> array( 'slug' => 'slide', 'with_front' => false ), /* you can specify its url slug */
    'has_archive' => 'slide', /* you can rename the slug here */
    'capability_type' => 'post',
    'hierarchical' => false,
    /* the next one is important, it tells what's enabled in the post editor */
    'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
    )/* end of options */
    );/* end of register post type */
    
    
}
// adding the function to the Wordpress init
add_action( 'init', 'plz_slide_custom_post');



add_filter( 'rwmb_meta_boxes', 'plz_slideshow_meta_boxes' );
function plz_slideshow_meta_boxes( $meta_boxes ) {
    // 1st meta box
    $meta_boxes[] = array(
        'title'      => __( 'Singola slide', 'palazzinatheme' ),
        'post_types' => 'slide',
        'fields' => array(
          array(
                            'name' => __( 'Immagine slide', 'palazzinatheme' ),
                            'id'   => 'plz_img_slide',
                            'type' => 'image_advanced',
                      ),
        )
    );
    return $meta_boxes;
}




/*****************
shortcode show slide_custom_post
*******************/

// create shortcode with parameters so that the user can define what's queried - default is to list all blog posts
add_shortcode( 'show_slideplz', 'sc_show_slide' );
function sc_show_slide( $atts ) {
    ob_start();
    // define attributes and their defaults
    extract( shortcode_atts( array (
    'post_type' => 'slide',
    'page_id' => '',
    ), $atts ) );
    // define query parameters based on attributes
    $options = array(
    'post_type' => $post_type,
    'page_id' => $page_id,
    );
    $query = new WP_Query( $options );
    // run the loop based on the query
    if ( $query->have_posts() ) { ?>


  <div class="box_slide_plz">
    <?php while ( $query->have_posts() ) : $query->the_post();
        ?>


        <div class="slideshow psgal" itemscope="" itemtype="http://schema.org/ImageGallery">


            <?php
    $images = rwmb_meta( 'plz_img_slide', 'type=image_advanced&size=max_width_1400' ); // Prior to 4.8.0
    $i=0;
    if ( !empty( $images ) ) {
        foreach ( $images as $id=>$image_big ) {
               ?>
               
             <div class="container-image-slider-homepage">
                <div class="image-slider-homepage" style="background-image:url('<?php echo $image_big['url'] ?>')"></div>
             </div>
                
        <?php
        }
    }
     ?>




        </div>

          <?php
        endwhile;
        wp_reset_postdata();
        ?>
  </div>

  <?php
        $myvariable = ob_get_clean();
        return $myvariable;
    }
}
?>