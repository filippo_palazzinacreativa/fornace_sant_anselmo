<?php
/*
Plugin Name: WooCommerce TM Extra Product Options Weight addon
Plugin URI: http://themecomplete.com/
Description: Addon to add weight support for radio buttons, checkboxes and select boxes
Version: 1.0.3
Author: themecomplete
Author URI: http://themecomplete.com/
*/

// Prevents direct file access
if ( ! defined( 'WPINC' ) ) {
    die;
}

/** Add the weight setting to radio buttons **/
function my_add_extra_choice( $options = array() ) {
    $options[] = array(
        "name"        => "weight",
        "label"       => __( 'Weight', 'woocommerce-tm-extra-product-options' ),
        "admin_class" => "tm_cell_display",
        "type"        => "radiobuttons",//selectbox , radiobuttons , checkboxes
        "field"       => array(
            "wpmldisable" => 1,
            "default"     => "",
            "type"        => "number",
            "tags"        => array( "class" => "t tm_option_display", "value" => "" ),
            "label"       => __( 'Weight', 'woocommerce-tm-extra-product-options' )
        )
    );
    $options[] = array(
        "name"        => "weight",
        "label"       => __( 'Weight', 'woocommerce-tm-extra-product-options' ),
        "admin_class" => "tm_cell_display",
        "type"        => "checkboxes",//selectbox , radiobuttons , checkboxes
        "field"       => array(
            "wpmldisable" => 1,
            "default"     => "",
            "type"        => "number",
            "tags"        => array( "class" => "t tm_option_display", "value" => "" ),
            "label"       => __( 'Weight', 'woocommerce-tm-extra-product-options' )
        )
    );
    $options[] = array(
        "name"        => "weight",
        "label"       => __( 'Weight', 'woocommerce-tm-extra-product-options' ),
        "admin_class" => "tm_cell_display",
        "type"        => "selectbox",//selectbox , radiobuttons , checkboxes
        "field"       => array(
            "wpmldisable" => 1,
            "default"     => "",
            "type"        => "number",
            "tags"        => array( "class" => "t tm_option_display", "value" => "" ),
            "label"       => __( 'Weight', 'woocommerce-tm-extra-product-options' )
        )
    );

    return $options;
}

add_filter( 'wc_epo_extra_multiple_choices', 'my_add_extra_choice', 50, 1 );

/** Save the weight setting to cart object **/
function wc_epo_add_cart_item_data_multiple( $array, $object ) {
    $key = array_search( $object->key, $object->element['option_values'] );
    if ( $key === null || $key === false ) {
        $key = false;
    }
    $weight = 0;
    switch ( $object->element['type'] ) {
        case "checkbox":
            if ( $key !== false && isset( $object->element['extra_multiple_choices'] ) && isset( $object->element['extra_multiple_choices']['weight'] ) && isset( $object->element['extra_multiple_choices']['weight'][ $key ] ) ) {
                $weight = $object->element['extra_multiple_choices']['weight'][ $key ];
            }
            break;
        case "radio":
            if ( $key !== false && isset( $object->element['extra_multiple_choices'] ) && isset( $object->element['extra_multiple_choices']['weight'] ) && isset( $object->element['extra_multiple_choices']['weight'][ $key ] ) ) {
                $weight = $object->element['extra_multiple_choices']['weight'][ $key ];
            }
            break;
        case "select":
            if ( $key !== false && isset( $object->element['extra_multiple_choices'] ) && isset( $object->element['extra_multiple_choices']['weight'] ) && isset( $object->element['extra_multiple_choices']['weight'][ $key ] ) ) {
                $weight = $object->element['extra_multiple_choices']['weight'][ $key ];
            }
            break;
        default:
            break;
    }

    $array['weight'] = $weight;

    return $array;
}

add_filter( 'wc_epo_add_cart_item_data_multiple', 'wc_epo_add_cart_item_data_multiple', 10, 2 );

/** Add the weight to the product **/
function add_custom_weight( $cart_object ) { 

    if ( !$cart_object || ( is_admin() && ! defined( 'DOING_AJAX' ) ) || $cart_object->is_empty() ) {
        return;
    }

    foreach ( $cart_object->get_cart() as $cart_item ) {
        if ( $cart_item['data']->tc_weight ){
            continue;
        }
        $extra_weight = 0;
        if ( isset( $cart_item['tmcartepo'] ) ) {
            foreach ( $cart_item['tmcartepo'] as $key => $value ) {
                if ( isset( $value['weight'] ) ) {
                    $quantity = 1;
                    if ( isset( $value['quantity'] ) ){
                        $quantity = floatval( $value['quantity'] );
                    }
                    $extra_weight = $extra_weight + ( $quantity * floatval( $value['weight'] ) );
                }
            }
        }
        if ( $cart_item['data'] && $extra_weight ){
            $weight = $cart_item['data']->get_weight();
            $extra_weight = floatval( $extra_weight );
            $weight = floatval( $weight );
            //var_dump($extra_weight + $weight);$extra_weight = 45;
            $cart_item['data']->set_weight( $extra_weight + $weight );
            //$cart_item['data']->set_weight( 10 );
            $cart_item['data']->tc_weight = 'added';
        }
    }

}

add_action( 'woocommerce_before_calculate_totals', 'add_custom_weight', 10, 1 );
