var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var livereload = require('gulp-livereload');
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');

var uglify = require('gulp-uglify');
var pump = require('pump');


var sassOptions = {
    errLogToConsole: true,
    outputStyle: 'expanded'
};

var autoprefixerOptions = {
    browsers: ['last 2 versions', '> 5%', 'Firefox ESR'],
    cascade: false
};

gulp.task('styles', function() {
    gulp.src('sass/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass(sassOptions).on('error', sass.logError))
        .pipe(autoprefixer(autoprefixerOptions))
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(sourcemaps.write('.'))

    .pipe(gulp.dest('./css/'))
        .pipe(livereload({ auto: true }));
});


gulp.task('compress', function(cb) {
    pump([
            gulp.src('js/*.js'),
            uglify(),
            gulp.dest('dist')
        ],
        cb
    );
});

//Watch task
gulp.task('default', function() {
    livereload.listen();
    gulp.watch('sass/*.scss', ['styles']);
    gulp.watch('js/*.js', ['compress']);
});