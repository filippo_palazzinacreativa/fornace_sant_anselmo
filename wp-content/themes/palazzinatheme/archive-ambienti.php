<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package palazzinatheme
 */

get_header(); ?>


	<div id="content-ambienti" class="content-page">

		<h1>Ambiente</h1>
		<h2>scegli l'ambientazione che più ti interessa</h2>


		<div id="lista-categorie-container">
			<?php
				$categories = get_categories( array(
				    'taxonomy' => 'categorie_ambienti'
				));
				foreach ($categories as $category) :?>
			<a href="<?= get_term_link( $category->term_id ); ?>" class="singola-categoria-ambiente-container">
				<div class="singola-categoria-ambiente">
					<?php $image_id = get_term_meta ( $category->term_id, 'categorie_ambienti-image-id', true ); echo wp_get_attachment_image ( $image_id, 'medium' );?>
					<div class="info-link-to-categoria-container">
						<div class="info-link-to-categoria">
							<div class="pre-title-categoria">
								Esempi di applicazione
							</div>
							<b class="nome-categoria-ambientazione">
								<?= $category->cat_name?>
							</b>
							<div class="button-vedi-esempi">
								vedi tutti gli esempi
							</div>
						</div>
					</div>
				</div>
			</a>
			<?php endforeach; ?>
		</div>
		<div class="clearfix">
	</div>


<?php
get_footer();
