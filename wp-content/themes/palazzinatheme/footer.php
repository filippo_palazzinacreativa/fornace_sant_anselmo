<?php
/**
* The template for displaying the footer.
*
* Contains the closing of the #content div and all content after.
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package palazzinatheme
*/

?>

  </div>
  <!-- end .main_container -->

  <footer>

    <div id="footer-content">
      <div class="row-footer">
        <div>
          <img src="<?php bloginfo('template_directory'); ?>/img/footer_logo.png" alt="logo-footer">
          <a href="http://www.santanselmo.it/" id="back-to-original-site-footer">torna al sito</a>
        </div>
      </div>
      <div class="row-footer">
        <div>Prodotti</div>
        <div class="menu-footer-container">
          <ul>
          <?php $terms = get_terms( 'product_tag' );
               
                if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
                    foreach ( $terms as $term ) {
                        echo '<li><a href="' . get_tag_link($term->term_id) . '">' . $term->name . '</a></li>';
                  }
              } 
          ?>
          </ul>
        </div>
      </div>
      <div class="row-footer">
        <div>naviga il sito</div>
        <div class="menu-footer-container">
          <?php wp_nav_menu( array( 'theme_location' => 'footer_naviga_sito' ) ); ?>
        </div>
      </div>
      <div class="row-footer">
        <div>restiamo in contatto</div>
        <p>
          Fornace S.Anselmo S.p.A.<br>
          via Tolomei, 61<br>
          35010 Loreggia (PD)<br>
          Italia<br>
          P.IVA IT02365150289<br>
          <br>
          Tel. +39 049 9304711<br>
          Fax: +39 049 9251217<br>
          E-mail: <a href="mailto:info@santanselmo.it?Subject=Richiesta%20informazioni%20dal%20sito">info@santanselmo.it</a>
        </p>
        <div id="social-footer-row">
          <a href="https://www.facebook.com/santanselmo/" id="facebook-logo" target="_blank">
            <div>
                <svg version="1.1"
                   xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/" x="0px" y="0px" width="36px" height="22px" viewBox="0 0 78.6 151.3" xml:space="preserve">
                  <style type="text/css">
                  .facebook-logo-svg{fill:#FFFFFF;}
                </style>
                <defs>
                </defs>
                <path id="f_1_" class="facebook-logo-svg" d="M51,151.3v-69h23.2l3.5-26.9H51V38.2c0-7.8,2.2-13.1,13.3-13.1l14.2,0V1.1C76.1,0.7,67.7,0,57.8,0
                  C37.3,0,23.2,12.5,23.2,35.6v19.8H0v26.9h23.2v69H51z"/>
                </svg>
            </div>
          </a>
          <a href="https://www.instagram.com/fornacesantanselmo/" id="instagram-logo" target="_blank">
            <div>
              <svg width="23px" height="23px" viewBox="0 0 23 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                  <defs></defs>
                  <g id="Desktop-HD-(1440x1020)" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                      <g id="Prodotto-interno" transform="translate(-1170.000000, -2639.000000)" fill="#FFFFFF" fill-rule="nonzero">
                          <g id="Group-10" transform="translate(0.000000, 2261.000000)">
                              <path d="M1181.56563,378.587107 C1178.57822,378.587107 1178.20376,378.599941 1177.03042,378.653107 C1175.85938,378.706732 1175.05959,378.892816 1174.36017,379.164607 C1173.63692,379.445566 1173.02322,379.821857 1172.41226,380.433274 C1171.80038,381.044691 1171.42409,381.658399 1171.14313,382.381649 C1170.87134,383.081524 1170.6848,383.880857 1170.63163,385.051899 C1170.57847,386.225232 1170.56563,386.599691 1170.56563,389.587107 C1170.56563,392.574524 1170.57847,392.948982 1170.63163,394.122316 C1170.68526,395.293357 1170.87134,396.093149 1171.14313,396.792566 C1171.42409,397.515816 1171.80038,398.129524 1172.4118,398.740482 C1173.02322,399.352357 1173.63692,399.728649 1174.36017,400.010066 C1175.06005,400.281399 1175.85938,400.467482 1177.03042,400.521107 C1178.20376,400.574274 1178.57822,400.587107 1181.56563,400.587107 C1184.55305,400.587107 1184.92751,400.574274 1186.10084,400.521107 C1187.27188,400.467482 1188.07167,400.281399 1188.77109,400.009607 C1189.49434,399.728649 1190.10805,399.352357 1190.71901,398.740941 C1191.33088,398.129524 1191.70717,397.515816 1191.98859,396.792566 C1192.25992,396.092691 1192.44601,395.293357 1192.49963,394.122316 C1192.5528,392.948982 1192.56563,392.574524 1192.56563,389.587107 C1192.56563,386.599691 1192.5528,386.225232 1192.49963,385.051899 C1192.44601,383.880857 1192.25992,383.081066 1191.98813,382.381649 C1191.70717,381.658399 1191.33088,381.044691 1190.71947,380.433732 C1190.10805,379.821857 1189.49434,379.445566 1188.77109,379.164607 C1188.07122,378.892816 1187.27188,378.706274 1186.10084,378.653107 C1184.92751,378.599941 1184.55305,378.587107 1181.56563,378.587107 Z M1181.56563,380.568941 C1184.50263,380.568941 1184.85051,380.580399 1186.01055,380.633107 C1187.08305,380.682149 1187.66513,380.861357 1188.05334,381.012149 C1188.56667,381.211524 1188.93334,381.449857 1189.31834,381.834857 C1189.70288,382.219857 1189.94122,382.586524 1190.14059,383.099857 C1190.29184,383.487149 1190.47059,384.069691 1190.51963,385.142191 C1190.57234,386.301774 1190.5838,386.650107 1190.5838,389.587107 C1190.5838,392.524107 1190.57234,392.871982 1190.51963,394.032024 C1190.47059,395.104524 1190.29138,395.686607 1190.14059,396.074816 C1189.94122,396.588149 1189.70288,396.954816 1189.31788,397.339816 C1188.93288,397.724357 1188.56622,397.962691 1188.05288,398.162066 C1187.66559,398.313316 1187.08305,398.492066 1186.01055,398.541107 C1184.85097,398.593816 1184.50263,398.605274 1181.56563,398.605274 C1178.62817,398.605274 1178.2803,398.593816 1177.12072,398.541107 C1176.04822,398.492066 1175.46613,398.312857 1175.07792,398.162066 C1174.56459,397.962691 1174.19792,397.724357 1173.81292,397.339357 C1173.42838,396.954357 1173.19005,396.587691 1172.99067,396.074357 C1172.83942,395.687066 1172.66067,395.104524 1172.61163,394.032024 C1172.55892,392.872441 1172.54747,392.524107 1172.54747,389.587107 C1172.54747,386.650107 1172.55892,386.302232 1172.61163,385.142191 C1172.66067,384.069691 1172.83988,383.487607 1172.99067,383.099399 C1173.19005,382.586066 1173.42838,382.219399 1173.81338,381.834399 C1174.19838,381.449857 1174.56505,381.211524 1175.07838,381.012149 C1175.46567,380.860899 1176.04822,380.682149 1177.12072,380.633107 C1178.2803,380.580399 1178.62863,380.568941 1181.56563,380.568941 Z M1181.56563,383.938607 C1178.44576,383.938607 1175.91713,386.467232 1175.91713,389.587107 C1175.91713,392.706982 1178.44576,395.235607 1181.56563,395.235607 C1184.68551,395.235607 1187.21413,392.706982 1187.21413,389.587107 C1187.21413,386.467232 1184.68551,383.938607 1181.56563,383.938607 Z M1181.56563,393.253774 C1179.54059,393.253774 1177.89897,391.612152 1177.89897,389.587107 C1177.89897,387.562063 1179.54059,385.920441 1181.56563,385.920441 C1183.59068,385.920441 1185.2323,387.562063 1185.2323,389.587107 C1185.2323,391.612152 1183.59068,393.253774 1181.56563,393.253774 Z M1188.75734,383.715399 C1188.75734,384.444415 1188.16636,385.035399 1187.43734,385.035399 C1186.70833,385.035399 1186.11734,384.444415 1186.11734,383.715399 C1186.11734,382.986383 1186.70833,382.395399 1187.43734,382.395399 C1188.16636,382.395399 1188.75734,382.986383 1188.75734,383.715399 Z" id="Instagram"></path>
                          </g>
                      </g>
                  </g>
              </svg>
            </div>
            <div></div>
          </a>
          <a href="https://it.pinterest.com/fornacesantanselmo/" id="pinterest-logo" target="_blank">
            <div>
              <svg width="23px" height="23px" viewBox="0 0 23 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <defs></defs>
                  <g id="Desktop-HD-(1440x1020)" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                      <g id="Prodotto-interno" transform="translate(-1221.000000, -2639.000000)" fill="#FFFFFF" fill-rule="nonzero">
                          <g id="Group-10" transform="translate(0.000000, 2261.000000)">
                              <path d="M1232.44986,378.587107 C1226.37556,378.587107 1221.4494,383.511899 1221.4494,389.587566 C1221.4494,394.090691 1224.15815,397.961316 1228.03519,399.662191 C1228.00402,398.894941 1228.02969,397.971399 1228.2254,397.135857 C1228.43715,396.243024 1229.64027,391.142691 1229.64027,391.142691 C1229.64027,391.142691 1229.28965,390.440066 1229.28965,389.401482 C1229.28965,387.771649 1230.23565,386.554316 1231.41173,386.554316 C1232.4109,386.554316 1232.8949,387.305982 1232.8949,388.206149 C1232.8949,389.211274 1232.25323,390.715982 1231.92323,392.108857 C1231.64823,393.277149 1232.50806,394.227732 1233.6594,394.227732 C1235.74344,394.227732 1237.14731,391.551066 1237.14731,388.379399 C1237.14731,385.969482 1235.52344,384.165024 1232.5704,384.165024 C1229.23419,384.165024 1227.15519,386.652399 1227.15519,389.431274 C1227.15519,390.391024 1227.43752,391.066149 1227.87981,391.589107 C1228.08423,391.830649 1228.11127,391.926441 1228.03748,392.203274 C1227.98569,392.404024 1227.86423,392.893066 1227.81381,393.085566 C1227.74048,393.363774 1227.51498,393.464607 1227.26381,393.361024 C1225.72611,392.733566 1225.01065,391.051024 1225.01065,389.158107 C1225.01065,386.034107 1227.64606,382.285399 1232.87198,382.285399 C1237.07215,382.285399 1239.83636,385.326899 1239.83636,388.587482 C1239.83636,392.903149 1237.4379,396.127066 1233.90002,396.127066 C1232.71386,396.127066 1231.59644,395.485399 1231.21327,394.756649 C1231.21327,394.756649 1230.57436,397.291232 1230.43961,397.779816 C1230.20677,398.627732 1229.75027,399.475649 1229.33273,400.135649 C1230.32227,400.428066 1231.36773,400.587107 1232.44986,400.587107 C1238.52461,400.587107 1243.4494,395.662316 1243.4494,389.587566 C1243.4494,383.511899 1238.52461,378.587107 1232.44986,378.587107" id="Pinterest"></path>
                          </g>
                      </g>
                  </g>
              </svg>
            </div>
          </a>
          <!--<a href="#">
            <div id="twitter-logo">
                <svg version="1.1"
                   xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/" width="34px" height="22px" viewBox="35 0 78.6 151.3" xml:space="preserve">
                    <style type="text/css">
                      .twitter-logo-svg{fill:#FFFFFF;}
                    </style>
                    <defs>
                    </defs>
                    <path class="twitter-logo-svg" d="M50.9,131.4c61.1,0,94.5-50.6,94.5-94.4c0-1.4,0-2.9-0.1-4.3c6.5-4.7,12.1-10.5,16.6-17.2
                  c-6,2.6-12.4,4.4-19.1,5.2c6.9-4.1,12.1-10.6,14.6-18.4c-6.4,3.8-13.5,6.6-21.1,8.1C130.2,4,121.6,0,112.1,0
                  C93.7,0,78.8,14.9,78.8,33.2c0,2.6,0.3,5.1,0.9,7.6C52.1,39.4,27.6,26.1,11.3,6.1C8.4,11,6.8,16.7,6.8,22.7
                  c0,11.5,5.9,21.7,14.8,27.6c-5.4-0.2-10.6-1.7-15-4.1c0,0.1,0,0.3,0,0.4c0,16.1,11.4,29.5,26.6,32.5c-2.8,0.8-5.7,1.2-8.8,1.2
                  c-2.1,0-4.2-0.2-6.2-0.6c4.2,13.2,16.5,22.8,31,23C37.8,111.7,23.5,117,7.9,117c-2.7,0-5.3-0.2-7.9-0.5
                  C14.7,125.9,32.1,131.4,50.9,131.4"/>
                </svg>
            </div>
          </a>
          <a href="#">
            <div id="youtube-logo">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/" x="0px" y="0px" width="22px" height="22px" viewBox="0 0 150 105.5" xml:space="preserve">
                <style type="text/css">
                  .youtube-logo-svg-triangolo{fill:transparent;}
                  .youtube-logo-svg{fill:#FFFFFF;}
                </style>
                <defs>
                </defs>
                <path id="Triangle_1_" class="youtube-logo-svg-triangolo" d="M59.5,72.2l40.5-21L59.5,30.1V72.2z"/>
                <g id="Lozenge_1_">
                  <g>
                    <path class="youtube-logo-svg" d="M148.5,22.8c0,0-1.5-10.3-6-14.9c-5.7-6-12.1-6-15-6.4C106.5,0,75,0,75,0H75c0,0-31.5,0-52.5,1.5
                      c-2.9,0.4-9.3,0.4-15,6.4c-4.5,4.6-6,14.9-6,14.9S0,34.9,0,47v11.4c0,12.1,1.5,24.3,1.5,24.3S3,93,7.5,97.6
                      c5.7,6,13.2,5.8,16.5,6.4c12,1.2,51,1.5,51,1.5s31.5,0,52.5-1.6c2.9-0.3,9.3-0.4,15-6.4c4.5-4.6,6-14.9,6-14.9s1.5-12.1,1.5-24.3
                      V47C150,34.9,148.5,22.8,148.5,22.8z M59.5,72.2l0-42.1L100,51.2L59.5,72.2z"/>
                  </g>
                </g>
                </svg>
            </div>
          </a>
          <a href="#">
            <div id="flickr-logo">
                <svg version="1.1"
                   xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
                   x="0px" y="0px" width="22px" height="22px" viewBox="0 0 151.5 72.5" xml:space="preserve">
                <style type="text/css">
                  .flickr-logo-svg{fill:#FFFFFF;}
                  .flickr-logo-svg-1{fill:#FFFFFF;}
                </style>
                <defs>
                </defs>
                <path class="flickr-logo-svg" d="M36.2,0C16.2,0,0,16.2,0,36.3c0,20,16.2,36.2,36.2,36.2c20,0,36.3-16.2,36.3-36.2C72.5,16.2,56.3,0,36.2,0"/>
                <path class="flickr-logo-svg-1" d="M115.3,0C95.2,0,79,16.2,79,36.3c0,20,16.2,36.2,36.3,36.2c19.3,0,35.1-15.1,36.2-34.2c0-0.7,0.1-1.4,0.1-2
                  C151.5,16.2,135.3,0,115.3,0"/>
                </svg>
            </div>
          </a>-->
        </div>
      </div>
    </div>

  </footer>
  <!-- end footer -->

  <!-- Root element of PhotoSwipe. Must have class pswp. -->
  <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
          <div class="pswp__container">
              <div class="pswp__item"></div>
              <div class="pswp__item"></div>
              <div class="pswp__item"></div>
          </div>
          <div class="pswp__ui pswp__ui--hidden">
              <div class="pswp__top-bar">
                  <div class="pswp__counter"></div>
                  <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                  <button class="pswp__button pswp__button--share" title="Share"></button>
                  <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                  <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                  <div class="pswp__preloader">
                      <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                          <div class="pswp__preloader__donut"></div>
                        </div>
                      </div>
                  </div>
              </div>
              <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                  <div class="pswp__share-tooltip"></div> 
              </div>
              <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
              </button>
              <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
              </button>
              <div class="pswp__caption">
                  <div class="pswp__caption__center"></div>
              </div>
          </div>
      </div>
  </div>
  <!-- END Root element of PhotoSwipe. Must have class pswp. -->

  <?php wp_footer(); ?>

    </body>

    </html>