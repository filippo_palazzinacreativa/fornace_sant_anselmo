<?php
/**
* palazzinatheme functions and definitions.
*
* @link https://developer.wordpress.org/themes/basics/theme-functions/
*
* @package palazzinatheme
*/

if ( ! function_exists( 'palazzinatheme_setup' ) ) :
    /**
* Sets up theme defaults and registers support for various WordPress features.
*
* Note that this function is hooked into the after_setup_theme hook, which
* runs before the init hook. The init hook is too late for some features, such
* as indicating support for post thumbnails.
*/
function palazzinatheme_setup() {
    /*
    * Make theme available for translation.
    * Translations can be filed in the /languages/ directory.
    * If you're building a theme based on palazzinatheme, use a find and replace
    * to change 'palazzinatheme' to the name of your theme in all the template files.
    */
    load_theme_textdomain( 'palazzinatheme', get_template_directory() . '/languages' );
    
    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );
    
    /*
    * Let WordPress manage the document title.
    * By adding theme support, we declare that this theme does not use a
    * hard-coded <title> tag in the document head, and expect WordPress to
    * provide it for us.
    */
    add_theme_support( 'title-tag' );
    
    /*
    * Enable support for Post Thumbnails on posts and pages.
    *
    * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
    */
    add_theme_support( 'post-thumbnails' );
    
    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
        'primary' => esc_html__( 'Primary', 'palazzinatheme' ),
        'footer_prodotti' => 'footer_prodotti',
        'footer_naviga_sito' => 'footer_naviga_sito',
        ) );
    
    
    
    /*
    * Switch default core markup for search form, comment form, and comments
    * to output valid HTML5.
    */
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
        ) );
    
    // Set up the WordPress core custom background feature.
    add_theme_support( 'custom-background', apply_filters( 'palazzinatheme_custom_background_args', array(
        'default-color' => 'ffffff',
        'default-image' => '',
        ) ) );
}
endif;
add_action( 'after_setup_theme', 'palazzinatheme_setup' );

//funzione che dichiara la compatibilità con woocommerce del tema
function mytheme_add_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

/**
* Set the content width in pixels, based on the theme's design and stylesheet.
*
* Priority 0 to make it available to lower priority callbacks.
*
* @global int $content_width
*/
function palazzinatheme_content_width() {
    $GLOBALS['content_width'] = apply_filters( 'palazzinatheme_content_width', 640 );
}
add_action( 'after_setup_theme', 'palazzinatheme_content_width', 0 );

/**
* Register widget area.
*
* @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
*/
function palazzinatheme_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'palazzinatheme' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Add widgets here.', 'palazzinatheme' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
        ) );
}
add_action( 'widgets_init', 'palazzinatheme_widgets_init' );



/**
* Enqueue scripts and styles.
*/
function palazzinatheme_scripts() {
    wp_enqueue_style( 'palazzinatheme-style', get_stylesheet_uri(), array(), filemtime( get_stylesheet_directory() ) );
    
    wp_enqueue_script( 'palazzinatheme-jquery', get_template_directory_uri() . '/dist/jquery-3.0.0.min.js', array(), '20151215', true );
    
    wp_enqueue_script( 'palazzinatheme-verticalcenter', get_template_directory_uri() . '/dist/jquery.flexverticalcenter.js', array(), '20151215', true );

    wp_enqueue_script( 'palazzinatheme-slick', get_template_directory_uri() . '../../../plugins/plz-slideshow-1.2/slick.min.js', array(), '20151215', true );
    
    wp_enqueue_script( 'palazzinatheme-matchHeight', get_template_directory_uri() . '/dist/jquery.matchHeight-min.js', array(), '20151215', true );

    wp_enqueue_script( 'palazzinatheme-mansory', get_template_directory_uri() . '/dist/mansory.js', array(), '20151215', true );
    
    wp_enqueue_script( 'palazzinatheme-customjs', get_template_directory_uri() . '/dist/custom.js', array(), '20151215', true );
    
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'palazzinatheme_scripts' );

/**
* Implement the Custom Header feature.
*/
require get_template_directory() . '/inc/custom-header.php';

/**
* Custom template tags for this theme.
*/
require get_template_directory() . '/inc/template-tags.php';

/**
* Custom functions that act independently of the theme templates.
*/
require get_template_directory() . '/inc/extras.php';

/**
* Customizer additions.
*/
require get_template_directory() . '/inc/customizer.php';

/**
* Load Jetpack compatibility file.
*/
require get_template_directory() . '/inc/jetpack.php';



/* Codice per modificare la lunghezza dell'excerpt by Roberto Iacono di robertoiacono.it */
function ri_excerpt_length($length) {
    return 20;
}
add_filter('excerpt_length', 'ri_excerpt_length');


function new_excerpt_more($excerpt) 
{
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');





/**
 * Inserts sidebar in a post or page
 *
 * Use: [get_sidebar name=""]
 *
 * @param      $name      Sidebar Slug
 *
 * @version 1.2
 * @author ENDif Media
 */
function do_midtext_sidebar($atts, $content = 'null'){
  extract( shortcode_atts(  array(
    'name' => 'sidebar-1',
    ), $atts ) );

    /**
     * extract() gives us all of our $atts as individual variables 
     * this is why we can call 'name' as $name. cool!
     */
    if (is_active_sidebar($name)){

        ob_start();
        dynamic_sidebar($name);
        $widgets = ob_get_contents();
        ob_end_clean();

        return $widgets;

    } else {

        //else return nothing
        return '';
        
    }

}
add_shortcode('get_sidebar','do_midtext_sidebar');


//wpsass_define_stylesheet( get_template_directory() . '/css/main.scss');

//woocommerce edit order element
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
add_action('woocommerce_before_single_product_summary', 'woocommerce_template_single_title');

function woocommerce_template_product_description() {
wc_get_template( 'single-product/tabs/description.php' );
}
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_product_description', 6 );

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

  unset( $tabs['description'] );   
  unset( $tabs['reviews'] );
  unset( $tabs['additional_information'] );
  
  return $tabs;

}

//rimosso prodotti correlati dalla pagina del singolo prodotto
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

//polylang traduzioni
$stringhe = array('Ambiente',"scegli l'ambientazione che più ti interessa", 'scopri il prodotto', '*la corrispondenza in metri quadri è considerata con giunta da 10mm di malta', 'per posare serve anche colla e malto, sicuro che non ti serve?', 'specifiche', 'istruzioni di posa e montaggio', 'spedizione', 'Cosa riceverai', 'Listello rosso ombrato caratterizzato per un aspetto leggermente rustico. Ottenuto da un processo speciale di invecchiamento e di un addizione di pigmenti naturali. Per una facciata dall’aspetto di un muro recuperato con marcata presenza di colore scuro.', 'Prodotti correlati', 'Listelli e angolari', '36 listelli', '18 angolari', 'Maggiori informazioni', 'termini e condizioni');

foreach($stringhe as $str){
    pll_register_string( 'palazzina_theme', $str);
}

//pdf prodotti
function add_custom_meta_boxes() {  
    add_meta_box('pdf_prodotto_specifiche', 'PDF Specifiche', 'pdf_prodotto_specifiche', 'product', 'normal', 'low');  
}
add_action('add_meta_boxes', 'add_custom_meta_boxes');  

function pdf_prodotto_specifiche() {  
    wp_nonce_field(plugin_basename(__FILE__), 'pdf_prodotto_specifiche_nonce');
    $html = '<p class="description">';
    $html .= 'Carica il Pdf delle specifiche qua.';
    $html .= '</p>';
    $html .= '<input type="file" id="pdf_prodotto_specifiche" name="pdf_prodotto_specifiche" value="" size="25">';
    $filearray = get_post_meta( get_the_ID(), 'pdf_prodotto_specifiche', true );
    if( $filearray ){
        $this_file = $filearray['url'];
        if($this_file != ""){
            $html .= '<div>Current file:<br>"<a href="'. $this_file .'" target="blank">' . $this_file . '"</a></div>';
        }
    }
    echo $html;
}

add_action('save_post', 'save_custom_meta_data');
function save_custom_meta_data($id) {
    if(!empty($_FILES['pdf_prodotto_specifiche']['name'])) {
        $supported_types = array('application/pdf');
        $arr_file_type = wp_check_filetype(basename($_FILES['pdf_prodotto_specifiche']['name']));
        $uploaded_type = $arr_file_type['type'];

        if(in_array($uploaded_type, $supported_types)) {
            $upload = wp_upload_bits($_FILES['pdf_prodotto_specifiche']['name'], null, file_get_contents($_FILES['pdf_prodotto_specifiche']['tmp_name']));
            if(isset($upload['error']) && $upload['error'] != 0) {
                wp_die('There was an error uploading your file. The error is: ' . $upload['error']);
            } else {
                update_post_meta($id, 'pdf_prodotto_specifiche', $upload);
            }
            add_post_meta($id, 'pdf_prodotto_specifiche', $upload);
        }
        else {
            wp_die("The file type that you've uploaded is not a PDF.");
        }
    }
}

function update_edit_form() {
    echo ' enctype="multipart/form-data"';
}
add_action('post_edit_form_tag', 'update_edit_form');

//script per il calcolo corretto del peso della spedizione
add_image_size( 'max_width_1400', 1400, 9999, false );
add_image_size( 'max_width_1024', 1024, 9999, false );

//funzione che si occupa di rendere funzionante la ricerca quando polylang è installato nel sito 
if ( is_plugin_active( 'polylang/polylang.php' ) ) {
    function fix_search_form_polylang( $html ) {
        $html = '<form class="search" method="get" action="'.esc_url( home_url('/') ).'" role="search">
                        <input type="search" class="live-search" name="s" placeholder="'.esc_attr__( '', 'slupy' ).'">
                        '.( defined( 'ICL_LANGUAGE_CODE' ) ? '<input type="hidden" name="lang" value="'.esc_attr( ICL_LANGUAGE_CODE ).'"/>' : '' ).'
                        <button type="submit" >

                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/" x="0px" y="0px" viewBox="0 0 57.8 92.6" style="enable-background:new 0 0 57.8 92.6;" xml:space="preserve">
                            <style type="text/css">
                                .send_search{fill:none;stroke:#B2B2B1;stroke-width:8;stroke-miterlimit:10;}
                            </style>
                            <defs>
                            </defs>
                            <polyline class="send_search" points="2.6,89.6 51.8,46.3 2.6,3 "/>
                        </svg>
                        
                        </button>
                        <svg id="close-ricerca" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/" x="0px" y="0px" viewBox="0 0 79.7 79.7" style="enable-background:new 0 0 79.7 79.7;" xml:space="preserve">
                            <style type="text/css">
                                .chiudi_ricerca{fill:none;stroke:#B2B2B1;stroke-width:8;stroke-miterlimit:10;}
                            </style>
                            <defs>
                            </defs>
                            <line class="chiudi_ricerca" x1="2.8" y1="2.8" x2="76.9" y2="76.9"/>
                            <line class="chiudi_ricerca" x1="76.9" y1="2.8" x2="2.8" y2="76.9"/>
                        </svg>
                        </form>';
        return $html;
    }
        
    //filter for search form
    add_filter( 'get_search_form', 'fix_search_form_polylang' );
}

//filtro per l'impostazione dei post type dove la ricerca trova risultati
function searchfilter($query) {
 
    if ($query->is_search && !is_admin() ) {
        $query->set('post_type',array('product', 'categorie_ambienti'));
    }
 
    return $query;

}
 
add_filter('pre_get_posts','searchfilter');

function mytheme_pre_get_posts( $query ) {
    if ( !is_admin() && $query->is_search() && $query->is_main_query() ) {
        $term = get_term_by('slug', get_query_var('s'), 'ambienti');
        if ($term) {
            $query->set( 'tax_query', array(
                 array(
                    'taxonomy' => 'ambienti',
                    'field'    => 'slug',
                    'terms'    =>  $term->slug,
                    'operator' => 'AND'
                )
            ));
         }
    }
}
add_action( 'pre_get_posts', 'mytheme_pre_get_posts', 1 );

function theme_search_where($where){
    global $wpdb;
  
    if ( is_search() )
      $where .= "OR (t.name LIKE '%".get_search_query() . "%' AND {$wpdb->posts} . post_status = 'publish')";
  
    return $where;
  }
  
  function theme_search_join($join){
    global $wpdb;
  
    if ( is_search() )
      $join .= "LEFT JOIN {$wpdb->term_relationships} tr ON {$wpdb->posts}.ID = tr.object_id INNER JOIN {$wpdb->term_taxonomy} tt ON tt.term_taxonomy_id=tr.term_taxonomy_id INNER JOIN {$wpdb->terms} t ON t.term_id = tt.term_id";
    return $join;
  }
  
  function theme_search_groupby($groupby){
    global $wpdb;
  
    // we need to group on post ID
    $groupby_id = "{$wpdb->posts} . ID";
    if ( ! is_search() || strpos($groupby, $groupby_id) !== false )
      return $groupby;
  
    // groupby was empty, use ours
    if ( ! strlen( trim($groupby) ) )
      return $groupby_id;
  
    // wasn't empty, append ours
    return $groupby . ", " . $groupby_id;
  }
  
  add_filter('posts_where', 'theme_search_where');
  add_filter('posts_join', 'theme_search_join');
  add_filter('posts_groupby', 'theme_search_groupby');

  function change_wp_search_size($query) {
    if ( $query->is_search ) 
        $query->query_vars['posts_per_page'] = 999; 

    return $query;
}
add_filter('pre_get_posts', 'change_wp_search_size'); 

//funzione che rimuove le voci in più dalla pagina utente registrato
function custom_my_account_menu_items( $items ) {
    unset($items['downloads']);
    unset($items['payment-methods']);
    return $items;
}
add_filter( 'woocommerce_account_menu_items', 'custom_my_account_menu_items' );

add_action( 'woocommerce_cart_updated', 'limit_cart_item_quantity', 10, 0 ); 
function limit_cart_item_quantity( ){

    $totale_cart = sprintf ( _n( '%d', '%d', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() );
    $quantita_carrello = WC()->cart->get_cart_item_quantities();

    $qunatita_pallet = 0;

   if( isset($quantita_carrello[803]) ){
        $qunatita_pallet = $quantita_carrello[803];

        $totale_cart = intval($totale_cart) - $qunatita_pallet;

        //ciclo per calcolare se presenti colla o malta
        foreach( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
            
            if($cart_item['tmcartepo'][0]['price'] > 0){
                
                $quantita_prodotto = $quantita_carrello[$cart_item['product_id']];

                $totale_cart += $quantita_prodotto;

            }
            
            if($cart_item['tmcartepo'][1]['price'] > 0){
                $quantita_prodotto = $quantita_carrello[$cart_item['product_id']];

                $totale_cart += $quantita_prodotto;
            }

        }

        $totale_pallet = intval( $totale_cart / 4 );

        foreach( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
            $product_id = $cart_item['data']->get_id();
            if( $product_id == 803 && $cart_item['quantity'] != $totale_pallet ){
                WC()->cart->set_quantity( $cart_item_key, $totale_pallet );
            }
        }

    }else{

        $totale_cart = sprintf ( _n( '%d', '%d', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() );
        $quantita_carrello = WC()->cart->get_cart_item_quantities();
    
        //ciclo per calcolare se presenti colla o malta
        foreach( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
            
            if($cart_item['tmcartepo'][0]['price'] > 0){
                
                $quantita_prodotto = $quantita_carrello[$cart_item['product_id']];

                $totale_cart += $quantita_prodotto;

            }
            
            if($cart_item['tmcartepo'][1]['price'] > 0){
                $quantita_prodotto = $quantita_carrello[$cart_item['product_id']];

                $totale_cart += $quantita_prodotto;
            }

        }

        $totale_pallet = intval( $totale_cart / 4 );

        if( $totale_pallet > 0)
            WC()->cart->add_to_cart(803);

    }

}

//rimozione script per interpretazione emoji
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

/**
 * funzione che crea un campo da aggiungere alla form di checkout
 *
 * @param object $checkout 
 * @return void
 */
function custom_woocommerce_billing_fields($fields)
{

    $fields['codice_fiscale_custom'] = array(
        'label' => 'Codice Fiscale',
        'placeholder' => '',
        'required' => true, 
        'type' => 'text',
    );

    return $fields;

}
//action di woocommerce per aggiungere un campo custom alla form di checkout
add_filter('woocommerce_billing_fields', 'custom_woocommerce_billing_fields');

/**
 * funzione che controlla se il campo è vuoto
 *
 * @return void
 */
function customise_checkout_field_process(){
	if (!$_POST['codice_fiscale_custom']) wc_add_notice(__('Non hai inserito il Codice Fiscale') , 'error');
}
add_action('woocommerce_checkout_process', 'customise_checkout_field_process'); 

/**
 * Funzone che aggiorna i campi cutom per woocommerce
 *
 * @param [type] $order_id
 * @return void
 */
function customise_checkout_field_update_order_meta($order_id)
{
	if (!empty($_POST['codice_fiscale_custom'])) {
		update_post_meta($order_id, 'codice_fiscale_custom', sanitize_text_field($_POST['codice_fiscale_custom']));
	}
}
add_action('woocommerce_checkout_update_order_meta', 'customise_checkout_field_update_order_meta');

/**
 * funzione che aggiunge un campo custom all'email di woocommerce
 *
 * @param [type] $fields
 * @param [type] $sent_to_admin
 * @param [type] $order
 * @return void
 */
function custom_woocommerce_email_order_meta_fields( $fields, $sent_to_admin, $order ) {
    $fields['codice_fiscale_custom'] = array(
        'label' => __( 'Codice fiscale' ),
        'value' => get_post_meta( $order->id, 'codice_fiscale_custom', true ),
    );
    return $fields;
}
add_filter( 'woocommerce_email_order_meta_fields', 'custom_woocommerce_email_order_meta_fields', 10, 3 );