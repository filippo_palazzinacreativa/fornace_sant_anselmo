<?php
/**
* The header for our theme.
*
* This is the template that displays all of the <head> section and everything up until <div id="content">
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package palazzinatheme
*/

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

  <?php wp_head(); ?>

  <!--font typekit-->
  <link rel="stylesheet" href="https://use.typekit.net/cmn2nqn.css">
  <link href="https://fonts.googleapis.com/css?family=Maven+Pro:400,700" rel="stylesheet">
  <!-- FACEBOOK CODE-->

  <body <?php body_class(); ?> id="body">
    <div class="site">

          <header>
            <div class="header_container container">
              <div class="logo_header">
                <a href="<?= pll_home_url( pll_current_language() ); ?>" title="<?=get_the_title(2)?>"><img src="<?php bloginfo('template_directory'); ?>/img/logo_sito.png"></a>
              </div>
              <div class="main-navigation">
                <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
                <div id="other-function-menu">
                  <div id="hmb_plz">
                    <span></span>
                    <span></span>
                    <span></span>
                  </div>
                  <span id="vertical-bar-menu"></span>
                  <div id="icon-function-menu">
                    <div id="search-button-menu">
                      <svg  viewBox="0 0 20 21" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                          <defs></defs>
                          <g id="Desktop-HD-(1440x1020)" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                              <g id="Prodotto-interno" transform="translate(-1299.000000, -24.000000)" fill="#B2B2B1" fill-rule="nonzero">
                                  <g id="Group-4" transform="translate(30.000000, 15.000000)">
                                      <g id="Group-2" transform="translate(1268.366469, 9.428433)">
                                          <path d="M15.9229822,13.2209558 L20.460017,17.7579907 L18.3386967,19.879311 L13.8016732,15.3422875 C12.4445765,16.2841671 10.7964562,16.8361648 9.01942765,16.8361648 C4.38173693,16.8361648 0.622047336,13.0764752 0.622047336,8.43878445 C0.622047336,3.80109373 4.38173693,0.0414041369 9.01942765,0.0414041369 C13.6571184,0.0414041369 17.416808,3.80109373 17.416808,8.43878445 C17.416808,10.2157806 16.8648304,11.8638733 15.9229822,13.2209558 Z M9.01942765,13.8361648 C12.0002641,13.8361648 14.416808,11.4196209 14.416808,8.43878445 C14.416808,5.45794798 12.0002641,3.04140414 9.01942765,3.04140414 C6.03859118,3.04140414 3.62204734,5.45794798 3.62204734,8.43878445 C3.62204734,11.4196209 6.03859118,13.8361648 9.01942765,13.8361648 Z" id="Combined-Shape"></path>
                                      </g>
                                  </g>
                              </g>
                          </g>
                      </svg>
                    </div>
                    <!--<div id="language-button-menu">
                      <div class="cur_lang"><?= pll_current_language() ?></div>
                      <ul class="list_lang"><?php pll_the_languages( array( 'hide_current' => 1, 'display_names_as' => 'slug' ) ); ?></ul>
                    </div>-->
                    <?php $totale_cart = sprintf ( _n( '%d', '%d', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() );

                          $quantita_carrello = WC()->cart->get_cart_item_quantities();
                          $qunatita_pallet = 0;

                          if( isset($quantita_carrello[835]) )
                            $qunatita_pallet = $quantita_carrello[835];

                          $totale_cart = intval($totale_cart) - $qunatita_pallet;
                    ?>
                    <div id="cart-button-menu" <?php if ($totale_cart > 0) {?>class="active-cart"<?php }?>>
                      <a href="<?= wc_get_cart_url()?>">
                      <svg width="18px" height="18px" viewBox="0 0 25 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                        <defs></defs>
                        <g id="Desktop-HD-(1440x1020)" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="carrello-ico" transform="translate(-1388.000000, -22.000000)">
                                <g id="Group-4" transform="translate(30.000000, 15.000000)">
                                    <g id="Group-15" transform="translate(1358.000000, 7.000000)">
                                        <path d="M16.8747722,11.5473435 L16.8747722,9.0145653 L21.2108397,9.0145653 L20.8444736,11.5473435 L16.8747722,11.5473435 Z M6.73859992,9.0145653 L10.5306967,9.0145653 L10.5306967,11.5473435 L7.16049177,11.5473435 L6.73859992,9.0145653 Z M10.5306967,5.20339772 L10.5306967,7.74408962 L6.7196589,7.74408962 C6.65128958,7.74408962 6.59589357,7.76316038 6.53401091,7.7805446 L6.10576214,5.20339772 L10.5306967,5.20339772 Z M11.793129,11.5473435 L15.6042966,11.5473435 L15.6042966,9.0145653 L11.793129,9.0145653 L11.793129,11.5473435 Z M11.793129,7.74408962 L15.6042966,7.74408962 L15.6042966,5.20339772 L11.793129,5.20339772 L11.793129,7.74408962 Z M21.7500102,5.20339772 L21.3901308,7.74408962 L16.8747722,7.74408962 L16.8747722,5.20339772 L21.7500102,5.20339772 Z M24.1718667,3.10340897 C23.9309524,2.82435319 23.5835273,2.67048981 23.218718,2.67048981 L5.67907017,2.67048981 L5.43802617,1.1890682 C5.33177481,0.575301176 4.80531813,0.129668176 4.18675098,0.129668176 L1.33028886,0.129668176 C0.629341232,0.129668176 0.0599429095,0.699066498 0.0599429095,1.40014386 C0.0599429095,2.09953469 0.629341232,2.67048981 1.33028886,2.67048981 L3.10815047,2.67048981 L5.46825397,16.8318892 C5.48083808,16.887415 5.50626575,16.9381406 5.52364998,16.9873094 C5.54907766,17.055549 5.5617915,17.1174317 5.59188956,17.1791846 C5.63638799,17.2664949 5.691784,17.3346048 5.74743948,17.4092013 C5.78376473,17.4583701 5.82190624,17.5074092 5.86471815,17.5502211 C5.93918491,17.6185904 6.02649525,17.6691863 6.11211906,17.7184848 C6.16284468,17.742226 6.19929966,17.7803675 6.25495514,17.7993085 C6.40388867,17.8548343 6.55930886,17.8912892 6.7196589,17.8912892 L20.6778964,17.8912892 C21.3774169,17.8912892 21.9483721,17.3283776 21.9483721,16.6209433 C21.9483721,15.9215525 21.3774169,15.3585111 20.6778964,15.3585111 L7.79812968,15.3585111 L7.58718375,14.0880354 L21.9483721,14.0880354 C22.579653,14.0880354 23.1124666,13.6233316 23.2061339,12.9969805 L24.4701229,4.1137699 C24.5256487,3.74727406 24.4129107,3.38259448 24.1718667,3.10340897 Z" id="Fill-1"></path>
                                        <path d="M10.5307097,21.06334 C10.5307097,22.1163831 9.67421217,22.9649669 8.62116907,22.9649669 C7.57448288,22.9649669 6.71967187,22.1163831 6.71967187,21.06334 C6.71967187,20.0166538 7.57448288,19.161713 8.62116907,19.161713 C9.67421217,19.161713 10.5307097,20.0166538 10.5307097,21.06334" id="Fill-4"></path>
                                        <path d="M21.9484239,21.06334 C21.9484239,22.1163831 21.0934832,22.9649669 20.046797,22.9649669 C18.9936242,22.9649669 18.1372564,22.1163831 18.1372564,21.06334 C18.1372564,20.0166538 18.9936242,19.161713 20.046797,19.161713 C21.0934832,19.161713 21.9484239,20.0166538 21.9484239,21.06334" id="Fill-7"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                        </svg>
                        <?php if ($totale_cart > 0) {?>
                        <div id="indication-number-prod">
                          <?= $totale_cart; ?>
                        </div>
                        <?php }?>
                        <div id="alert-add-product">
                          <div>
                            Prodotto aggiunto al carrello
                          </div>
                        </div>
                      </a>
                    </div>
                    <div id="account-icon">
                      <a href="<?= get_permalink(8)?>">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/" x="0px" y="0px"viewBox="0 0 106.2 101.2" style="enable-background:new 0 0 106.2 101.2;" xml:space="preserve">
                          <style type="text/css">
                              .account_icon{fill:none;stroke:#B2B2B1;stroke-width:8;stroke-miterlimit:10;}
                          </style>
                          <defs>
                          </defs>
                          <circle class="account_icon" cx="53.3" cy="28.6" r="24.6"/>
                          <path class="account_icon" d="M73.2,65.7H33c-16,0-29,13-29,29v2.5h98.2v-2.5C102.2,78.7,89.2,65.7,73.2,65.7z"/>
                          </svg>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
            <!-- end .header_container-->
            <div id="cerca-header-container">
              <div id="container-relative">
                  <?php get_search_form()?>
              </div>
            </div>
          </header>
          <!-- end header-->
          

          <div class="main_container">

         