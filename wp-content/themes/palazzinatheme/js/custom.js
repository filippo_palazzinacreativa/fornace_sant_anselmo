 var jQuery_1_12_4 = jQuery.noConflict(true);

jQuery( document ).ready(function() {

	//slider ambientazioni pagina prodotto
	jQuery_1_12_4('#slider-ambienti-prodotto').slick({
		autoplay: true,
    	dots: false,
    	touchMove: false,
    	draggable:false,
    	autoplaySpeed:3500,
    	prevArrow:'',
    	nextArrow:'',
	});

	//sincronizzo stampa prezzo nell'elemento custom
	jQuery('.variations_form').on('change' ,function(){

		if(jQuery(".tm-final-totals > .price").text() != ""){
			jQuery("#total-price-custom-js span").html(jQuery(".tm-final-totals > .price").text());
		}
		
	});

	//cambia immagine prodotto select malta
	jQuery("#select-tipo-malta-pagina-prodotto select").on('change', function(){

		set_image_product_for_malta(this);

	});

	//controllo quantita
	jQuery('#plus-quantity').on('click', function () {

		jQuery('.qty').val( parseInt(jQuery('.qty').val()) + 1 );

		set_testo_qty_above( parseInt(jQuery('.qty').val()) );
		
		//codice per aggiornare il prezzo del totale nella pagina del singolo prodotto
		jQuery("#total-price-custom-js span").html( parseFloat( parseFloat(jQuery(".tm-unit-price > .price").text().slice(0,-1).replace(",",".")).toFixed(2) * jQuery('.qty').val() ).toFixed(2).replace(".",",") + "€" );

	});

	jQuery('#less-quantity').on('click', function () {

		if( parseInt(jQuery('.qty').val()) > 1 ){

			jQuery('.qty').val( parseInt(jQuery('.qty').val()) - 1 );

			set_testo_qty_above( parseInt(jQuery('.qty').val()) );

			//codice per aggiornare il prezzo del totale nella pagina del singolo prodotto
			jQuery("#total-price-custom-js span").html( parseFloat( parseFloat(jQuery(".tm-unit-price > .price").text().slice(0,-1).replace(",",".")).toFixed(2) * jQuery('.qty').val() ).toFixed(2).replace(".",",") + "€" );
		}

	});

	//controlli slider
	jQuery('#prev-button-slider').on('click', function () {
		jQuery_1_12_4('#slider-ambienti-prodotto').slick('slickPrev');
	});

	jQuery('#next-button-slider').on('click', function () {
		jQuery_1_12_4('#slider-ambienti-prodotto').slick('slickNext');
	});

	jQuery_1_12_4('#slider-ambienti-prodotto').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		jQuery("[data-slider-position = '"+ currentSlide + "']").removeClass('active-slide');
		jQuery("[data-slider-position = '"+ nextSlide + "']").addClass('active-slide');
	});

	//menu controller
	jQuery("#hmb_plz").on('click', function(){

	  if( jQuery("#hmb_plz").hasClass("active-menu") ){
	    jQuery(".menu-menu-principale-container").removeClass("open-menu-mobile");
	    jQuery("#hmb_plz").removeClass("active-menu");
	  }else{
	    jQuery(".menu-menu-principale-container").addClass("open-menu-mobile");
	    jQuery("#hmb_plz").addClass("active-menu");
	  }

	});

	jQuery('#product-space .single_add_to_cart_button').addClass("check-value-select-malta");

	//gestione aggiungi al carrello
	jQuery('#product-space .single_add_to_cart_button').on('click', function(e){

		if( jQuery( this ).hasClass("check-value-select-malta") ){

			if( jQuery('.select-malta option:selected' ).attr('data-price') == 0 ){

				e.preventDefault();

				jQuery("#messaggio-avviso-malta").slideDown();

				jQuery('#product-space .single_add_to_cart_button').removeClass('check-value-select-malta');

			}

		}else{

			jQuery('#product-space form.cart').submit();

		}

	});

	jQuery('.woocommerce-product-gallery__wrapper a').on('click', function( e ){

		e.preventDefault();

	});

	set_image_product_for_malta(jQuery("#select-tipo-malta-pagina-prodotto select"));

	jQuery(".info-prodotto .singola-tipologia").mouseenter( function(){
		jQuery(this).siblings().removeClass("active-price-list-product");
		jQuery( this ).addClass("active-price-list-product");
		jQuery( this ).parent().siblings('.prezzo-prodotto-correlato').html( jQuery(this).attr("data-price-product") );
	});


	//cambio dicitura delle spedizioni per peso nella pagina del checkout
	if( jQuery(".cart-collaterals .shipping").length ){

		jQuery(".cart-collaterals .shipping").each( function() {
			var text = jQuery(this).html();
			text = text.replace("Weight Based Shipping", "Preventivo di spedizione basato sul peso");
			jQuery(this).html(text);
		});

		jQuery('.woocommerce-shipping-calculator').submit(function() {

			setTimeout(function(){
				jQuery(".cart-collaterals .shipping").each( function() {
					var text = jQuery(this).html();
					text = text.replace("Weight Based Shipping", "Preventivo di spedizione basato sul peso");
					jQuery(this).html(text);
				});
			}, 1500);
			
		});
	
	}

});//document ready


jQuery( window ).load( function(){

	resize_slider_ambienti_prodotto();

	//trigger click listelli load pagina
	jQuery( '.tawcvs-swatches .selected' ).trigger('click');
	jQuery( '.tawcvs-swatches .swatch-scatola_di_36_listelli' ).trigger('click');
	
});

jQuery( window ).resize(function() {
    setTimeout(resize_slider_ambienti_prodotto(), 500);
});

//resize slider ambienti singolo prodotto
function resize_slider_ambienti_prodotto(){
	jQuery("#slider-ambienti-prodotto").height(jQuery("#product-space").height());
}

//codice photoswype
(function($) {
    var $pswp = $('.pswp')[0];
    var image = [];

    $('#slider-ambienti-prodotto').each( function() {
        var $pic     = $(this),
            getItems = function() {
                var items = [];
                //loop immagini
                $pic.find('.img-back-container').each(function() {
                    var $href   = $(this).attr('data-img'),
                        $size   = $(this).data('size').split('x'),
                        $width  = $size[0],
                        $height = $size[1];

                    var item = {
                        src : $href,
                        w   : $width,
                        h   : $height
                    }

                    items.push(item);
                });
                return items;
            }

        var items = getItems();

        $.each(items, function(index, value) {
            image[index]     = new Image();
            image[index].src = value['data-img-thumbnail'];
        });

        //bottone per lanciare apertura photoswype
        jQuery('#zoom-image').on( 'click', function(event) {

            var $index = jQuery_1_12_4('#slider-ambienti-prodotto').slick('slickCurrentSlide');
            var options = {
                index: $index,
                bgOpacity: 1,
                showHideOpacity: true
            }

            var lightBox = new PhotoSwipe($pswp, PhotoSwipeUI_Default, items, options);
            lightBox.init();
        });
	});
	
	//effetto scroll pagina 
	jQuery('.single-prodotto-correlato').each(function(i){

		var element = jQuery(this); 

		setTimeout(function(){
			top_load_market(element);
		}, 0, element);

	});

	set_testo_qty_above(1);

})(jQuery);

//funzione che imposta l'immagine della malta come immagine del prodotto
function set_image_product_for_malta(select_malta){
	var image = jQuery('option:selected', select_malta).attr('data-imagep');
	jQuery('.woocommerce div.product div.images .woocommerce-product-gallery__image').html('<img src="' + image + '">');
}

jQuery(window).on('load', function(){
	//effetto mansory
	jQuery_1_12_4('#lista-categorie-container').masonry({
		// set itemSelector so .grid-sizer is not used in layout
		itemSelector: '.singola-categoria-ambiente-container',
		columnWidth: '.singola-categoria-ambiente-container',
		percentPosition: true,
	});

	jQuery_1_12_4('#lista-prodotti-correlati .single-prodotto-correlato .info-prodotto-correlato .info-prodotto b').matchHeight();
	jQuery_1_12_4('#body #container-pagina-cerca .page_search .sing_result_link > div .title_prod_link').matchHeight();

	//preload immagini per pagina prodotto
	if( jQuery(".tm-product-image").length ){

		jQuery(".tm-product-image > option").each( function(){
			jQuery('<img />').attr('src',jQuery(this).attr("data-imagep")).appendTo('body').css('display','none');
		});

	}
});


//effetto scroll prodotti
jQuery( window ).scroll(function() {

	jQuery('.single-prodotto-correlato').each(function(i){

		var element = jQuery(this); 

		  setTimeout(function(){
			  top_load_market(element);
		  }, 0 , element);

	});

});
//funzioneche calcoal se un elemento è in vista
jQuery.fn.isInViewport = function() {
	var elementTop = jQuery(this).offset().top;
	var elementBottom = elementTop + jQuery(this).outerHeight();

	var viewportTop = jQuery(window).scrollTop();
	var viewportBottom = viewportTop + jQuery(window).height();

	return elementBottom > viewportTop && elementTop < viewportBottom;
};

function top_load_market(element){
	if ( element.isInViewport() && !element.hasClass('complete-animation-image-prodotto') ) {
		element.addClass('active-scroll');
		element.find('div.replace-src').css('background-image', 'url(' + element.find('div.replace-src').attr('data-src')  + ')');

		setTimeout(function(){
			element.addClass('complete-animation-image-prodotto');
		}, 400, element);
	}
}

//funzione che gestisce la stringa di testo sotto la quantità nella pagina di un singolo prodotto
function set_testo_qty_above( qty ){
	if ( jQuery( ".single-product .swatch-scatola_di_18_angolari" ).hasClass('selected') ){
		var valore = 1.17 *  parseInt(jQuery('.qty').val());
		jQuery("#dicitura-quantita > span").html(valore.toFixed(2) + " ML");
	}else if ( jQuery( ".single-product .swatch-scatola_di_36_listelli" ).hasClass('selected') ){
		var valore = 0.60 *  parseInt(jQuery('.qty').val());
		jQuery("#dicitura-quantita > span").html(valore.toFixed(2) + " MQ");
	}
}

jQuery( '.single-product .swatch-label' ).on('click', function(){
	setTimeout(function(){
		set_testo_qty_above(parseInt(jQuery('.qty').val()));
	}, 300);
} );

jQuery( '#search-button-menu' ).on( 'click', function(){

	jQuery('#cerca-header-container').toggleClass('active-search');

});

jQuery('#close-ricerca').on( 'click', function(){

	jQuery('#cerca-header-container').toggleClass('active-search');

});

jQuery( '.cart' ).on('submit', function(ev){

	if( !jQuery('#alert-add-product').hasClass('active-alert-cart') ){

		ev.preventDefault();

		jQuery('#alert-add-product').addClass('active-alert-cart');

		jQuery(this).submit();
	
	}

});