<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package palazzinatheme
 */

get_header();?>



		<?php
		if ( have_posts() ) : ?>


<div id="container-pagina-cerca">
<div class="page_search">
    <h1><?php printf( esc_html__( 'Risultati ricerca per: %s', 'palazzinatheme' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
    <div class="container">
    
    <div class="content_page">

            <?php
                /* Start the Loop */
                while ( have_posts() ) : the_post(); 

                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );

                if( $post->post_type == 'product' ){ ?>
                    <a href="<?php the_permalink(); ?>" class="sing_result_link">
                        <div>
                            <div class="title_prod_link" ><?php the_title( ); ?></div>
                            <?php echo '<img src="' . $image[0] . '">'; ?>
                            <div class="pseudo-button-search-result">Vedi pagina</div>
                        </div>
                    </a>
                <?php  }else{ ?>

                <?php $cate = get_the_terms( $post->term_id, 'categorie_ambienti' );?>
                
                    <a href="<?php echo get_term_link($cate[0]->slug, 'categorie_ambienti'); ?>" class="sing_result_link">
                        <div>
                            <div class="title_prod_link" ><?= $cate[0]->name; ?></div>
                            <?php echo '<img src="' . $image[0] . '">'; ?>
                            <div class="pseudo-button-search-result">Vedi pagina</div>
                        </div>
                    </a>
                    <?php 
                break;
            ?>

            <?php
                }
                endwhile;

                the_posts_navigation();

            else :?>

                <div id="container-pagina-cerca">
                <div class="page_search">
                    <h1><?php printf( esc_html__( 'Nessun risultato per la ricerca: %s', 'palazzinatheme' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
            
<?php
            endif; ?>

        </div>
    </div>
</div>
        </div>
<?php
//get_sidebar();
get_footer();
