<?php
/**
* The template for displaying all single posts.
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
*
* @package Studio_BMA
*/

get_header(); ?>


  <div class="content_page single_page_news">

    <?php
while ( have_posts() ) : the_post();?>

      <?php $feat_image_url = wp_get_attachment_url( get_post_thumbnail_id() );?>



        <div id="head_page">
          <img src="<?php echo $feat_image_url ?>">
        </div>


        <div class="box_single_news">
          <!--<h4>Pubblicato il <?php the_time('Y-m-d'); ?></h4>-->
          <?php the_content(); ?>

            <div class="clear"></div>
        </div>




            <?php	
endwhile; // End of the loop.
?>
  </div>



  <?php
//get_sidebar();
get_footer();