<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package palazzinatheme
 */

get_header(); ?>

	<div id="content-ambienti" class="content-page content-page-singoli-ambienti">
		<?php
		if ( have_posts() ) : ?>

		<h1><?= single_term_title()?></h1>
		<h2><?php pll_e("scegli l'ambientazione che più ti interessa"); ?></h2>
			
		<div id="lista-categorie-container">
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

					$ambiente_img = get_the_post_thumbnail_url( get_the_ID() );

					$id_prodotto_collegato = get_post_meta($post->ID, 'plz_ambiente_prodotto', true);

			?>

				<a class="singola-categoria-ambiente-container"  href="<?= get_permalink($id_prodotto_collegato);?>">
					<div class="singola-categoria-ambiente">
						<img src="<?= $ambiente_img;?>">
						<div class="info-link-to-categoria-container">
							<div class="info-link-to-categoria">
								<div class="button-vedi-esempi">
									<?php pll_e('scopri il prodotto'); ?>
								</div>
							</div>
						</div>
						<div class="velina-hover-prodotti-ambienti">
							<div class="nome-prodotto-ambiente">
								<div>
									<?= get_the_title($id_prodotto_collegato); ?>
								</div>
							</div>
						</div>
					</div>
				</a>
				
			<?php
			endwhile;

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>


		

			
		</div>
		<div class="clearfix"></div>
	</div>

<?php
get_footer();