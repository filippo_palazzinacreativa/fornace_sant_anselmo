<?php
/* Template Name: Template Home */

get_header(); 
?>

<div id="homepage-container">
	<div class="slider-container">
		<?php echo do_shortcode( '[show_slideplz page_id=“15"]' );?>
	</div>
	<div id="container-text-container">
		<?php the_content();?>
		<div id="container-passaggi-lista">
			<div class="container-passaggio">
				<div class="img-container-passaggi">
					<img src="<?php bloginfo('template_directory'); ?>/img/homepage_passo_1.png">
					<div class="vertical-align-fix"></div>
				</div>
				<p>1. Scegli ciò che ti serve</p>
			</div>
			<div class="container-passaggio">
				<div class="img-container-passaggi">
					<img src="<?php bloginfo('template_directory'); ?>/img/homepage_passo_2.png">
					<div class="vertical-align-fix"></div>
				</div>
				<p>2. Ricevi il kit pronto all'uso</p>
			</div>
			<div class="container-passaggio">
				<div class="img-container-passaggi">
					<img src="<?php bloginfo('template_directory'); ?>/img/homepage_passo_3.png">
					<div class="vertical-align-fix"></div>
				</div>
				<p>3. Segui le istruzioni per la posa in opera</p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<a href="<?= get_permalink(5)?>" id="left-product-link">
		<div class="image-background-square-homepage" id="prodotto-homepage-square" style="background-image:url('<?= get_the_post_thumbnail_url( 5, 'max_width_1024' ) ?>')"></div>
		<div class="link-square-homepage">
			<span>scegli per</span>
			<span>
				Prodotto
				<img src="<?php bloginfo('template_directory'); ?>/img/arrow_right_link.png">
			</span>
		</div>
	</a>

	<a href="<?= get_post_type_archive_link('ambienti')?>" id="right-ambient-link">
		<div class="image-background-square-homepage" id="ambiente-homepage-square"></div>
		<div class="link-square-homepage">
			<span>scegli per</span>
			<span>
				Ambiente
				<img src="<?php bloginfo('template_directory'); ?>/img/arrow_right_link.png">
			</span>
		</div>
	</a>
	<div id="black-square-under-link-box-home"></div>
</div><!-- homepage-container -->

<?php
get_footer();
