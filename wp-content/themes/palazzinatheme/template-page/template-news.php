<?php
/* Template Name: Template News */

get_header(); ?>


<div id="head_page"><h1>LE NOSTRE NEWS</h1></div>

  <div class="news">
    <div class="post_progetti">


      <?php
while ( have_posts() ) : the_post();?>


		<?php
			$args = array(
			'post_type' => 'post',
			'post_status' => 'publish',
			'posts_per_page' => 1000,
			'tax_query' => array(
			array(
			'taxonomy' => 'category',
			'field'    => 'slug',
			'terms'    => 'news',
			)
			)
			);
			$the_query = new WP_Query( $args );
			while ( $the_query->have_posts() ) : $the_query->the_post();
			?>


        <a href="<?php the_permalink(); ?>" class="single_news_progetto">
          <div class="single_data_progetto">
            <p>
              <?php the_time('j') ?>
            </p>
            <p>
              <?php the_time('F') ?>
            </p>
            <p>
              <?php the_time('Y') ?>
            </p>
          </div>
          <div class="single_txt_progetto">
            <?php the_title( '<h3>', '</h3>' ); ?>
              <?php the_excerpt( '<p>', '</p>' ); ?>
          </div>
        </a>

		<?php
			endwhile;
			?>



        <?php

endwhile; // End of the loop.
?>

    </div>

<div class="sidebar">

	  <?php
$terms = get_terms('progetti');
echo '<ul>';
foreach ($terms as $term) {
    echo '<li><a href="'.get_term_link($term).'">'.$term->name.'</a></li>';
}
echo '</ul>';
?>
</div>

<div class="clear"></div>
  </div>

  <?php
get_footer();