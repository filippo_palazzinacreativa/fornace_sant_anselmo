<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

	<div id="homepage-container" class="product-page-version">
		<div class="slider-container">
			<?php echo do_shortcode( '[show_slideplz page_id=“15"]' );?>
		</div>
		<div id="container-text-container">
			<?php $post_content = get_post(2);
					$content = $post_content->post_content;
				  echo do_shortcode($content);
			?>
			<div id="container-passaggi-lista">
				<div class="container-passaggio">
					<div class="img-container-passaggi">
						<img src="<?php bloginfo('template_directory'); ?>/img/homepage_passo_1.png">
						<div class="vertical-align-fix"></div>
					</div>
					<p>1. Scegli ciò che ti serve</p>
				</div>
				<div class="container-passaggio">
					<div class="img-container-passaggi">
						<img src="<?php bloginfo('template_directory'); ?>/img/homepage_passo_2.png">
						<div class="vertical-align-fix"></div>
					</div>
					<p>2. Ricevi il kit pronto all'uso</p>
				</div>
				<div class="container-passaggio">
					<div class="img-container-passaggi">
						<img src="<?php bloginfo('template_directory'); ?>/img/homepage_passo_3.png">
						<div class="vertical-align-fix"></div>
					</div>
					<p>3. Segui le istruzioni per la posa in opera</p>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div><!-- homepage-container -->

		<?php if ( have_posts() ) : ?>
				<div id="container-prodotti-correlati" class="product-list">
					<div id="prodotti-correlati">
							
							<div id="lista-prodotti-correlati">

				<?php 
					
					$count=0;
					while ( have_posts() ) : the_post(); 

					$image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'single-post-thumbnail' );
					$variation = new WC_Product_Variable(get_the_ID());

					?>

								<a href="<?= get_permalink()?>" class="single-prodotto-correlato">
									<div class="back-animation-scroll-product">
										<div class="container-img-prodotto-correlato">
											<?php if( $variation->is_on_sale() ) echo '<div class="in-offerta">In offerta</div>';?>
											<div class="replace-src" data-src="<?= $image[0];?>"></div>
											<div class="fade-back-animation"></div>
										</div>
										<div class="info-prodotto-correlato">
											<div class="info-prodotto">
												<b><?= the_title();?></b>
												<span>Listelli e angolari</span>
												<div class="tipologie-prodotto-correlato">
													
													<?php	

													
													$variables = $variation->get_available_variations(); 

													

													?>

													<div class="singola-tipologia active-price-list-product" data-price-product="<?php if( $variables[1]['display_regular_price'] != $variables[1]['display_price'] ) echo '<span>€ '.number_format((float)$variables[1]['display_regular_price'], 2, '.', '') .'</span> € '. number_format((float)$variables[1]['display_price'], 2, '.', ''); else echo "€ " . number_format((float)$variables[1]['display_regular_price'], 2, '.', ''); ?>">
														36 listelli
													</div>
													<div class="singola-tipologia" data-price-product="<?php if( $variables[0]['display_regular_price'] != $variables[0]['display_price'] ) echo '<span>€ '.number_format((float)$variables[0]['display_regular_price'], 2, '.', '') .'</span> € '. number_format((float)$variables[0]['display_price'], 2, '.', ''); else echo "€ " . number_format((float)$variables[0]['display_regular_price'], 2, '.', ''); ?>">
														18 angolari
													</div>

												</div>
												<div class="prezzo-prodotto-correlato">
													<?php if( $variables[1]['display_regular_price'] != $variables[1]['display_price'] ) echo '<span>€ '.number_format((float)$variables[1]['display_regular_price'], 2, '.', '') .'</span> € '. number_format((float)$variables[1]['display_price'], 2, '.', ''); else echo "€ " . number_format((float)$variables[1]['display_regular_price'], 2, '.', ''); ?>
												</div>
												<div class="maggiori-dettagli-button">Maggiori informazioni</div>
											</div>
										</div>
										<div class="animation-container"></div>
									</div>
								</a>

				<?php $count++; endwhile; // end of the loop. ?>

							</div>
						</div>
						<div class="clearfix"></div>
					</div>


		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php
				/**
				 * woocommerce_no_products_found hook.
				 *
				 * @hooked wc_no_products_found - 10
				 */
				do_action( 'woocommerce_no_products_found' );
			?>

		<?php endif; ?>


<?php get_footer( 'shop' ); ?>
