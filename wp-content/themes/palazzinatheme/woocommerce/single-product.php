<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

		<?php while ( have_posts() ) : the_post(); ?>


			<div id="header-prodotto">

				<div id="product-space">

					<?php wc_get_template_part( 'content', 'single-product' ); ?>

					<div id="dicitura-indicazione-giunta">
						<?php pll_e('*la corrispondenza in metri quadri o metri lineari è considerata con giunta da 10mm di malta'); ?>
					</div>

					<div id="messaggio-avviso-malta">
						<?php pll_e('per posare serve anche colla e malto, sicuro che non ti serve?'); ?>
					</div>

					<div id="lista-pulsanti-link-prodotto">
						<?php $pdf_specifiche = get_post_meta( get_the_ID(), 'pdf_prodotto_specifiche', true ); if( !$pdf_specifiche ) $pdf_specifiche['url'] = ""; ?>
						<div>
							<a href="<?= $pdf_specifiche['url'] ?>" target="_blank">
								<div class="vertical-align-fix"></div><div class="titolo-altre-pagina-prodotto"><?php pll_e('specifiche'); ?></div>
							</a>
						</div>

						<div id="central-element-altre-pagine-prodotto">
							<a href="<?= get_site_url() . '/pdf/Istruzioni_Posa_Listelli.pdf'?>" target="_blank">
								<div class="vertical-align-fix"></div><div class="titolo-altre-pagina-prodotto"><?php pll_e('istruzioni di posa e montaggio'); ?></div>
							</a>
						</div>

						<div>
							<a href="<?= get_permalink(795); ?>" target="_blank">
								<div class="vertical-align-fix"></div><div class="titolo-altre-pagina-prodotto"><?php pll_e('termini e condizioni'); ?></div>
							</a>
						</div>

					</div>

				</div>

				<div id="ambiente-space">
					<?php 

					$id_prodotto = get_the_ID();

					$query_ambienti = new WP_Query(
						array(
						    'post_type' => 'ambienti',
						    'posts_per_page' => -1,
						));

					$array_img_ambienti = array();

					while ($query_ambienti->have_posts()) {
					    $query_ambienti->the_post();
					    $post_id = get_the_ID();
					    
					    $id_prodotto_collegato = get_post_meta($post->ID, 'plz_ambiente_prodotto', true);

					    if( $id_prodotto_collegato == $id_prodotto )
					    	$array_img_ambienti []= wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large');
					}

					wp_reset_query();

					$counter_slider = 0;

					?>


					<div id="slider-ambienti-prodotto" itemscope itemtype="http://schema.org/ImageGallery">

					<?php foreach($array_img_ambienti as $k=>$image_single){?>
						<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
							<div class="img-back-container" data-img="<?= $image_single[0];?>" itemprop="contentUrl" data-size="1024x1024">
								<div class="image-back-ambiente" data-img-thumbnail="<?= $image_single[0];?>" style="background-image:url('<?= $image_single[0];?>')"></div>
							</div>
						</figure>
					<?php $counter_slider++; }?>

					</div>
					<div id="control-slider-container">
						<div id="control-slider">
							<div id="zoom-image"><img src="<?php bloginfo('template_directory'); ?>/img/plus_zoom_image.png"></div>
							<div id="prev-button-slider"><img src="<?php bloginfo('template_directory'); ?>/img/left_arrow_slider.png"></div>
							<div id="container-pagination-slider">
								<?php for( $i=0; $i < $counter_slider; $i++ ): ?>
									<div data-slider-position="<?=$i?>" class="single-image-pagination-slider <?php if( $i == 0 ) echo "active-slide";?> "><span></span></div>
								<?php endfor?>
							</div>
							<div id="next-button-slider"><img src="<?php bloginfo('template_directory'); ?>/img/right_arrow_slider.png"></div>
						</div>
					</div>
				</div>
			</div>

		<?php endwhile; // end of the loop. ?>

	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	<div style="clear:both"></div>
	<div id="esempio-packaging-container">
		<div id="esempio-packaging">
			<h3><?php pll_e('Cosa riceverai'); ?></h3>
			<p><?php pll_e('All’interno della tua confezione troverai, insieme ai mattoni, un kit pronto all’uso contenente colla e malta necessari per la posa del prodotto'); ?></p>
			<div id="cosa-riceverai-container">
				<img src="<?php bloginfo('template_directory'); ?>/img/cosa-riceverai_left.png">
				<img src="<?php bloginfo('template_directory'); ?>/img/cosa-riceverai_right.png">
			</div>
		</div>
	</div>

	<div id="container-prodotti-correlati">
		<div id="prodotti-correlati">
			<h4><?php pll_e('Prodotti correlati'); ?></h4>
			<div id="lista-prodotti-correlati">
				<?php
					//prelevo i prodotti correlati
					$array_prodotti_correlati = wc_get_related_products( get_the_ID(), 4 );

					foreach ($array_prodotti_correlati as $key => $id_prodotto_collegato_visualizzato) :

						$prodotto = wc_get_product( $id_prodotto_collegato_visualizzato );
						//immagine prodotto
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $id_prodotto_collegato_visualizzato ), 'single-post-thumbnail' );

				?>
				<a class="single-prodotto-correlato" href="<?= get_permalink($id_prodotto_collegato_visualizzato)?>">
					<div class="back-animation-scroll-product">
						<div class="container-img-prodotto-correlato">
							<div class="replace-src" data-src="<?= $image[0];?>"></div>
							<div class="fade-back-animation"></div>
						</div>
						<div class="info-prodotto-correlato">
							<div class="info-prodotto">
								<b><?= $prodotto->get_title()?></b>
								<span><?php pll_e('Listelli e angolari'); ?></span>
								<div class="tipologie-prodotto-correlato">
								<?php	

									$variation = new WC_Product_Variable($id_prodotto_collegato_visualizzato);
									$variables = $variation->get_available_variations(); 

									?>

									<div class="singola-tipologia active-price-list-product" data-price-product="€ <?= substr(strip_tags($variables[1]['price_html']), 0, -6)?>">
										36 listelli
									</div>
									<div class="singola-tipologia" data-price-product="€ <?= substr(strip_tags($variables[0]['price_html']),0,-6)?>">
										18 angolari
									</div>
								</div>
								<div class="prezzo-prodotto-correlato">
									€ <?= substr(strip_tags($variables[1]['price_html']), 0, -6)?>
								</div>
								<div class="maggiori-dettagli-button"><?php pll_e('Maggiori informazioni'); ?></div>
							</div>
						</div>
						<div class="animation-container"></div>
					</div>
				</a>
				<?php endforeach; ?>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>

	<?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		//do_action( 'woocommerce_sidebar' );
	?>

<?php get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
